package car_dealership;

public class Employee {

   public void handleCustomer(Customer customer, boolean finance, Vehicle vehicle){
       if(finance){
           double loadAmount = vehicle.getPriceOfVehicle() - customer.getCashOnHand();
           runCreditHistory(customer, loadAmount);
       } else if(vehicle.getPriceOfVehicle() <= customer.getCashOnHand()){
           //customer pays in cash
           processTransaction(customer, vehicle);
       } else {
           System.out.println("Customer will need more money to purchase vehicle: " +vehicle);
       }

   }

   public void runCreditHistory(Customer customer, double loanAmount){
       System.out.println("Ran credit history for Customer...");
       System.out.println("Customer has been approved to purchase the vehicle");
   }

   public void processTransaction(Customer customer, Vehicle vehicle){
       System.out.println("Customer has purchased the vehicle: "+vehicle+" for the price "+vehicle.getPriceOfVehicle());
   }

}


//    public static void handleCustomer(Customer customer, boolean finance, Vehicle vehicle){
//        if(finance == true){
//            double doubleAmount;
//            runCreditHistory(customer, doubleAmount);
//            else if (vehicle.getPriceOfVehicle() <= customer.getCashOnHand())
//                processTransaction(Customer customer, Vehicle vehicle){
//                System.out.println("Customer " + customer+" has bought a new "+vehicle);
//            }
//            else {
//                System.out.println("tell customer to bring more money");
//            }
//        }
//
//    }
//
//
//    public static void processTransaction(Customer customer, Vehicle vehicle) {
//        System.out.println("Customer " + customer + " has bought a new " + vehicle);
//    }
//
//    public static void runCreditHistory(Customer customer, double doubleAmount){
//        doubleAmount = customer.getCashOnHand();
//
//
//    }
