package car_dealership;

public class Customer {

    private String name;
    private String address;
    private double cashOnHand;

    public Customer(String name, String address, double cashOnHand) {
        this.name = name;
        this.address = address;
        this.cashOnHand = cashOnHand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        address += " Dealership Adress";
        this.address = address;
    }

    public double getCashOnHand() {
        return cashOnHand;
    }

    public void setCashOnHand(double cashOnHand) {
        cashOnHand += 500;   // we give bonus 500
        this.cashOnHand = cashOnHand;
    }

    public void purchaseCar(Vehicle vehicle, Employee employee, boolean finance){
        employee.handleCustomer(this, finance, vehicle);

    }

//    public double getCashOnHand(double cashOnHand){      //returns the amount of money that customer has
//        return(getCashOnHand());
//    }
}
