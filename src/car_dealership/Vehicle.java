package car_dealership;

import java.util.Objects;

public class Vehicle {

    private String make;
    private String modelOfVehicle;
    private double priceOfVehicle;

    public Vehicle(String make, String modelOfVehicle, double priceOfVehicle) {
        this.make = make;
        this.modelOfVehicle = modelOfVehicle;
        this.priceOfVehicle = priceOfVehicle;
    }

    public String getModelOfVehicle() {
        return modelOfVehicle;
    }

    public void setModelOfVehicle(String modelOfVehicle) {
        this.modelOfVehicle = modelOfVehicle;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public double getPriceOfVehicle() {
        return priceOfVehicle;
    }

    public void setPriceOfVehicle(double priceOfVehicle) {
        this.priceOfVehicle = priceOfVehicle;
    }

    @Override                            // it is good to implement toString method - right click and generate
    public String toString() {
        return "Vehicle{" +
                "make='" + make + '\'' +
                ", modelOfVehicle='" + modelOfVehicle + '\'' +
                ", priceOfVehicle=" + priceOfVehicle +
                '}';
    }

    @Override                            // right click and generate - equals and hashCode  -- without this method we will not get desired result because .equals will compare hastags...not the model, make, price...
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return Double.compare(vehicle.getPriceOfVehicle(), getPriceOfVehicle()) == 0 && Objects.equals(getMake(), vehicle.getMake()) && Objects.equals(getModelOfVehicle(), vehicle.getModelOfVehicle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMake(), getModelOfVehicle(), getPriceOfVehicle());
    }
}
