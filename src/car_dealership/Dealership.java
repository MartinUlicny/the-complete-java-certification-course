package car_dealership;

public class Dealership {

    public static void main(String[] args) {

        Customer customer1 = new Customer("Tom", "123 Anything St.", 25000);

        Vehicle vehicle1 = new Vehicle("BMW", "M3", 20000);

        Employee employee1 = new Employee();

        customer1.purchaseCar(vehicle1, employee1, false);

        Vehicle car = new Vehicle("BMW", "M3", 20000);

        boolean value = car.equals(vehicle1);
        System.out.println("Are the cars matches? Answer is: "+value);

    }

}
